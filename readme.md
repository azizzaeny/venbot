
# Venbot - Chatbot Vending Machine
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](mailto:zie.azlance@gmail.com)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)  <br>

## About    
> "Chatbot Vending machine,     
>  What if we build chatbot for vending machine"     

## Table of content    
- [About](#About)
- [Status](#status)
- [Requirements and Dependencies](Requirements#%201%20and#%201%20Dependencies)
- [Project setup](#Project#%201%20setup)
- [How we build it (dev blogs)](#How#%201%20we#%201%20build#%201%20it#%201%20(dev#%201%20blogs))
  * [1. Whats is the plan?](#1.#%201%20Whats#%201%20is#%201%20the#%201%20plan?)
  * [2. Infromation model](#2.#%201%20Infromation#%201%20model) 
  * [3. Inserting coins](#3.#%201%20Inserting#%201%20coins)
  * [4. Selecting items](#Selecting#%201%20items)
  * [5. Purchasing](#5.#%201%20Purchasing)
  * [6. Return coins](#6.#%201%20Return#%201%20coins)
  * [7. Events system](#7.#%201%20Events#%201%20system)
  * [8. Rendering](#8.#%201%20Rendering)

## Status 

The project was planned in a week sprint, but somehow it endup almost two weeks now ;p  

**Planned Start date**:  02/02/2020 - 07/02/2020, 6 days   


**The Actual Sprint** :   

| Descriptions                         | date                      | notes                        |
| ------------------------------------ | ------------------------- | ---------------------------- |
| Research about Vending               | february, 02 - 03, 1days  | planned 3days, but only 1day |
| Start implementing core domain       | february, 06 - 07, 2days  | late implementation          |
| Epic struggle with the return coins  | february, 08       1days  | stop, continued on monday    |
| Finishup core and test it            | february, 10 - 12, 3days  | finished milestone1          |



**Products_name**        : Venbot  
**Tags**                 : Vending machine challanges   
**Target Milestone**     :  
- `1. Core model, the state up to the events system and all unit testing `,
- `2. Improve rendering, chat bots, commands list, improve user experiences, and overall flow purchase`, 
- `3. Maintenance mode, refill stock, vault/deposit balance`, 
- `4. Extract dataset transaction to be used in machine laerning, build summarize, reports and statistic data, more querying about interesting information about how dialy vending works`,


	
**Evaluation**           :     
- Notes - improvement    
   - Improve user interface and user experiences of the chat bots with python curses interface   
   
   
## Requirements and Dependencies 
- python 3.7 
- pip 
- pytest 
- toolz 


## Project Setup 
1. `git clone https://github.com/azizzaeny/venbot`
2. `python3 (from the repl then explore the world)`

**File Structure**:   
```
- venbot_core.py -> core domain functions, no side effect, only information mode, pure functions
- venbot_test.py -> unit test 
- venbot.py      -> side effects, printing, raw_input, main program
```


```
**Running it from docker***   
**docker python***   
```


------------------------------------------------------------------------------------- 

## How we build it - (dev blogs)
#### 1. So whats is the plan?
> Build chatbots connected apps, accepting an input and render it in the console or terminal messaging clients       
>   
> Users will input text, that text would be tokenize so the chatbots can understand whats going on there,         
> Build simple tokenize string, split string into an array take only head, second and maybe thirds arguments then mapped all into the commands rules and matrix.     
    

Chatbots design should be auditable by implementing data driven events,    
so we can rebuild past state by reconstructing the events from the past    

The hardest part was how we can pull and extract statistic data to build predictive vending model from typical vending transaction events       

**Example questions?**    

- On sundays, what was items users mostly buys?   
- In the peek transactions days, what time users usually purchase, is it afternoon, or in the morning?    
- How many users in single transaction user usually buys an items, how much coins they used?    
- Ho much time and efforts users interact with the system    
- How the seasson effect, the temp effect, is it cold drinks on the hot days?   
- What item that has been inserted there, but not yet purchased after the last resupply    
- Is there any pattern that we can learn from location forecast supply point, sales, so insufficient  items and coins can be resupply on demand and scheduled     

**Example Commands** :   

**1. Inserting coins**   
```
add-coins 500 
add-coins 100 
add-coins 10 
1 100 

# 1 is add-coins 100 is coins value 
```
**2. Selecting items**   
```
select-items 1 2 
select-items Canned Drinks 3
choose 1 x 2
2 3
undo-items 1 
cancel-items 1

# 2 is selecting an items and the last args would be quantity items
```
**3. Purchasing**     

```
confirm 
purchase
ok
get items
get it 
```

**4. Returning Coins**  
```
return my coins
exchange
change coins 
return 
```

**5. Status and statistic**    
```
show me the status 
show products 
show items
show me possible coins
show coins 
what is the accepted coins
```
**6. Maintenance commands**   

```
vault balance 
withdraw balance
deposit balance 
refill items 
restock coins
collect coins
summary 
reports
```

#### 2. Information model  

**2.1. Model the state**    
```python
{
    'spec':{
      'coins':[],
      'items':[],
      'coins_empty_rules':{},
      'coins_limit_rules':{},
      'coins_exchange_rules':[],
      'commands':{},
      'vm_descriptions':{}
    },
    'db': {
      'events':[],
      'transactions':[],
      'stats': [],
      'chats': [],
      'collected_coins':[],
      'logs':[['initiated']]
    },
    'vm': {
      'maintenance': False,
      'funds_coins':[],
      'funds_amounts':0,
      'predicted_funds': 0,
      'locked': False,
      'selected_items': [],
      'recommended_items':{},
      'return_gate':[], 
	  'outlet': [],    
      'iteraction_time':0,
      'started_time':0,
    }
  }
```
> The state consist of 3 aspect the spec, db and the vm, spec was the specification and rules of how the system should behave     
> db was the persistent data to be saved to storage, it can be read from file and reconstructed again,   
> The vm was the running state of single interaction with the vending machine system    

```python
from toolz.curried import *
from operator import *
import uuid 
import json 
import time
import uuid
import random
import math

def initial_state():
  'create initial state of the vending consisting of spec, db, vm'
  return {
    'spec':{
      'coins':[],
      'items':[],
      'coins_empty_rules':{},
      'coins_limit_rules':{},
      'coins_exchange_rules':[],
      'commands':{},
      'vm_descriptions':{}
    },
    'db': {
      'events':[],
      'transactions':[],
      'stats': [],
      'chats': [],
      'collected_coins':[],
      'logs':[['initiated']]
    },
    'vm': {
      'maintenance': False,
      'funds_coins':[],
      'funds_amounts':0,
      'predicted_funds': 0,
      'locked': False,
      'selected_items': [],
      'recommended_items':{},
      'return_gate':[], #for coins 
      'outlet': [],     #for items purchased last 
      'iteraction_time':0,
      'started_time':0,
    }
  }
```

**2.2. Create coins**     

> Coins Information Model   

```python
# coins
{
  "id": "9214b929ae014d4f990a549040db3b4d",
  "value": 100,
  "currency": "RP",
  "qty": 100,
  "alias": [
    "cepe"
  ],
  "signature": {
    "weight": 0,
    "diameter": 0,
    "thickness": 0
  }
}
```
> Id was unique uuid, value is the denomination value of the currencies,  
> Alias used for chatbots if the users type alias into the chat system    
> What is the signature?, well the real vending machines read coins from it sizes, diameter, etc, probably we gonna used it later on leave it there  

```python
def guid():
  'return uuid string str(uuid.uuid4())'
  return uuid.uuid4().hex

def coins(value, currency='Rp', options={'alias':[], 'qty':0, }, signature={'weight':0, 'diameter':0, 'thickness':0}):
  'create coins with given value with currency and empty quantity'
  return merge({
    'id' : guid(),
    'value': value,
    'currency': currency
  }, options, {'signature': signature})
```   


> Lets test the coins    


```python 
def test_coins():
  'test coins creation'
  coins_with_value = coins(10)
  coins_currency_jpy = coins(10, 'JPY')
  coins_options = coins(100, {'alias': ['dim', 'yen'], 'qty':0 })
  coins_signature = coins(10, 'JPY', {'qty':20, 'alias': 'ten'}, {'weight':'10g', 'diameter':'5mm', 'thickness': '3mm'})
  
  assert coins_with_value['value'] == 10, 'create coins with single value'
  assert coins_currency_jpy['currency'] == 'JPY', 'coins currency JPY'
  assert coins_options['qty'] == 0
  assert coins_signature['signature'] ==  {'weight': '10g', 'diameter': '5mm', 'thickness': '3mm'}
```   

> Multiple array creation of coins    

```python 
def create_coins(*coins_list_val_curr_qty_limt):
  'quick create coins with given of currency the rest arguments list is the coins value'
  def map_coins(i):
    [value, currency, qty, signature, alias] = i
    if signature == 0:
      signature = {'weight':0, 'diameter':0, 'thickness':0}
    if alias == 0:
      alias = []
    return coins(value, currency, {'qty': qty, 'alias':alias}, signature)
  
  return list(map(map_coins, coins_list_val_curr_qty_limt))

def coins_yen():
 'create coins yens with given array tupple list'
 coins_list  = create_coins(
   [10, 'JPY', 100, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
   [50, 'JPY', 200, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
   [100, 'JPY', 300,{'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
   [500, 'JPY', 500,{'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])
 return coins_list

```   

> Test create_coins   

```python

coins_list = create_coins([100, 'RP', 100, 0, ['cepe']],
                          [500, 'RP', 200, 0, ['gope']],
                          [1000, 'RP', 300, 0, ['serebu']],
                          [5000, 'RP', 500, 0, ['goceng']])

coins_list_given_signature = create_coins(
  [10, 'JPY', 10, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
  [50, 'JPY', 20, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
  [100, 'JPY', 9, {'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
  [500, 'JPY', 3, {'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])

limit_rules =  { '10': 9, '100': 3}

coins_list_yen  = create_coins(
    [10, 'JPY', 10, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
    [50, 'JPY', 0, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
    [100, 'JPY', 2,{'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
    [500, 'JPY', 10,{'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])

coins_list_yen2  = create_coins(
  [10, 'JPY', 10, 0, 0],
  [50, 'JPY', 0, 0, 0],
  [100, 'JPY', 3, 0, 0],
  [500, 'JPY', 10, 0, 0])

def test_create_coins():
  'test create quick multiple coins arguments'
  'check wheater the value of each coins is correct'
  assert list_value(coins_list) == [100, 500, 1000, 5000]
  assert list_value_of('signature', coins_list_given_signature)[0] ==  {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}
  
```   



**2.3. Create items**    

> Items Model   


```python 
{
  "id": "bfccb9193da14c629ce6c008a6bf0e17",
  "name": "Canned Coffee",
  "number": 1,
  "prices": 120,
  "qty": 800,
  "tags": [
    "canned drinks"
  ]
}
```

> Items Functions    


```python
def items(no, name, prices=0, qty=0, options={'tags': [], 'alias': []}):
  'create items with given name prices and options'
  return merge({
    'id': guid(),
    'name': name,
    'number': no, 
    'prices': prices,
    'qty': qty
  }, options)
```   

> Create quick multi items    


```python

def create_items(*list_name_prices_qty):
  'quick create multiple of items'
  def map_items(i):
    [no, name, prices, qty, options] = i
    if options == 0:
      options = {'tags': [], 'alias':[] }      
    return items(no, name, prices, qty, options)

  return list(map(map_items, list_name_prices_qty))

def describe_spec_items(state={}):
  'create specs items for our vending machine'
  items_list = create_items(
    [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
    [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
    [3, 'Chicki', 50, 600, {'tags':['snacks']}],
    [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
    [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
    [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  
  return assoc_in(state, spec_items, items_list)
```   


> Test items    


```python   
def test_items():
  'test create items'
  items_no_name = items(1, 'Canned')
  items_prices  = items(2, 'Coffee', 1000)
  items_with_qty = items(3, 'Coffee', 1000, 10)
  assert items_no_name['name'] == 'Canned'
  assert items_no_name['number'] == 1
  assert items_no_name['prices'] == 0, 'should be zero prices if not given prices'
  assert items_prices['prices'] == 1000
  assert items_with_qty['qty'] == 10
  
items_list = create_items(
  [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
  [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
  [3, 'Chicki', 50, 600, {'tags':['snacks']}],
  [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
  [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
  [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  
def test_create_items():
  'test create multiple items at onces'
  assert items_list[0]['number'] == 1
  assert items_list[0]['name'] == 'Canned Coffee'

def test_items_spec_creation():
  'test items spec of our state'
  spc = describe_spec_items({'foo':'bar'})
  assert is_empty(spc['spec']['items']) == False
```   

**2.4. Vm spec**    
  
```python
def spec_vending_machine(location=''):
  'describe the spec used by system' 
  return {
    'location_address': location,
    'model_number': 'FGG1XXMCY8',
    'coins_capacity': 5000,
    'items_capacity': 562,
    'no_item_selection': 32,
    'output_temperature':'0-23c',
    'voltage_power': '220-240v/50hz',
    'number_rack_col': 18,
    'dimension':'H 1834 * W 882 * D 852 mm',
    'weight': '197kg',
    'supported_payment': ['Coin bill'], #creditcard,
    'interaction_mode': ['button','chatbots app']}
```  

> Note: above spec only used for items capacity and coins_capacity] the rest information was not yet used for building our predictive vm model    


```python
def assign_initial_spec(coins_sp={}, items_sp={}, state ={}):
  'quick assign initial spec to the state, make the system ready to accept coins coins rules and items'
  if is_empty(state):
    state = initial_state() 
  if is_empty(coins_sp):
    coins_list   = coins_yen()
  else:
    coins_list   = coins_sp 

  empty_coins_yen_rules = { '10': [10, 50, 100, 500], '100': [500] }
  limit_coins_yen_rules = { '10': 9, '100': 4} 
  exchanges_rules = [100, 10]
  
  if is_empty(items_sp):
    items_list = create_items(
      [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
      [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
      [3, 'Chicki', 50, 600, {'tags':['snacks']}],
      [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
      [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
      [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  else:
    items_list = items_sp
    
  value_spec_vending_machine = spec_vending_machine()
  return thread_first(
    state,
    (assoc_in, spec_coins, coins_list),
    (assoc_in, spec_coins_empty_rules, empty_coins_yen_rules),
    (assoc_in, spec_coins_limit_rules, limit_coins_yen_rules),
    (assoc_in, spec_items, items_list),
    (assoc_in, spec_coins_exchange_rules, exchanges_rules),
    (assoc_in, spec_vm_description, value_spec_vending_machine))
```   

```python
  empty_coins_yen_rules = { '10': [10, 50, 100, 500], '100': [500] }
  limit_coins_yen_rules = { '10': 9, '100': 4} 
  exchanges_rules = [100, 10]
  # create dynamic rules for the vms, it can be changed, the algoritm should not be bundled into a functions _
```   

> Assigning the state we required specification added to the system   
>    
> 1. Assign coins    
> 2. Desicribe items   
> 3. Assign vending spec - used for coins capacity, items limits, and selection limit  
> 4. Assign exchange rule - used for returning coins    
> 5. Assign limit rules and empty coins rules - eg:if coins 100 is out of stock then coins 500 not able to used    
   


> Test assigning inital spec    


```python
  
def test_items_spec_creation():
  'test items spec of our state'
  spc = describe_spec_items({'foo':'bar'})
  assert is_empty(spc['spec']['items']) == False


def test_initial_spec():
  'test if valid specs of the state or assigned crucial aspec of the state'
  inits = assign_initial_spec()
  assert is_not_empty(get_in(spec_coins, inits)) == True, 'should contains some keys and vals from given specs'
  assert is_not_empty(get_in(spec_items, inits)) == True
  assert is_not_empty(get_in(spec_coins_empty_rules, inits)) == True
  assert is_not_empty(get_in(spec_coins_limit_rules, inits)) == True


def test_description_of_vending_machine():
  'test its is available no_selection, coins_capacity, items_capacity'
  assert spec_vending_machine('')['no_item_selection'] == 32
  assert spec_vending_machine('')['coins_capacity'] == 5000
  assert spec_vending_machine('')['items_capacity'] == 562

```
  
**2.5. Commands**    
   
   
#### 3. Inserting coins    
> We want to a insert a coins to the vm, but there is a couple things that we need to checks    
>    
> 1. The coins inserted should be available in the system    
> 2. The coins should not out of stock, limit bigger than qty of coins and qty > zero  
> 3. The coins is not blacklisted, meaning the coins is not able to used due to another coins of shortage  
> 4. The inserted coins qty is predicted available in the system, qty should not meet the limit of the vm capability   
> 5. Last check is weather vm system able to acceting more coins, predicted intended qty coins to insert + count all available qty coins in the system + collected coins  < limit of coins vm capability   
>  
> If all those functions passed then build chat matrix, consisting of [0,0,0,0,0] == passed   
> eg: [0,0,1,1,1]  the system whould read and understand these matrix and able to give output to the chats according ly   
>  
> The flow would be like this    
> Filter -> build chat matrix -> update inserted_funds_coins, -> recalculate funds-> amounts   
>    
> The return would be transformed new state    


**3.1 Insert Coins**  


```python
@curry
def insert_coins(coins_value, inserted_coins_qty, state):
  'return new state with added new inserted coins to the spec_coins if all the check are valid'
  
  limit_rules                        = get_in(spec_coins_limit_rules, state, {})
  empty_rules                        = get_in(spec_coins_empty_rules, state, {})
  source_coins                       = get_in(spec_coins, state, [])
  current_funds_coins                = get_in(vm_funds_coins, state, [])
  vm_capacity                        = get_in(spec_coins_capacity, state, math.inf)
  collected_coins                    = get_in(db_collected_coins, state, [])
  
  total_coins_in_spec                = total_coins_in_list(source_coins)
  blacklisted_coins                  = get_blacklisted_coins(limit_rules, empty_rules, source_coins)

  pass_coins_by_value                  = find_coins_by('value', coins_value, source_coins)
  pass_coins_not_out_of_stock          = find_coins_is_available(limit_rules, pass_coins_by_value) #so we can make a changes 
  pass_coins_is_not_blacklisted        = find_coins_is_not_blacklisted(blacklisted_coins, pass_coins_not_out_of_stock)
  pass_coins_available_qty             = find_coins_by_predicted_qty(inserted_coins_qty, vm_capacity, pass_coins_is_not_blacklisted)
  pass_coins_capacity_check            = list(filter(lambda _: is_accepting_more_coins(inserted_coins_qty,collected_coins ,total_coins_in_spec, vm_capacity),
                                                     pass_coins_available_qty))
  
  #intended_qty, collected_coins, all_coins_qty, vm_capacity
  pass_coins_value                     = maps_value(pass_coins_capacity_check)

  chat_matrix                          = [
    int(is_empty(pass_coins_by_value)),
    int(is_empty(pass_coins_not_out_of_stock)),
    int(is_empty(pass_coins_is_not_blacklisted)),
    int(is_empty(pass_coins_available_qty)),
    int(is_empty(pass_coins_capacity_check)),
  ]
  #intended_qty, collected_coins, all_coins_qty, vm_capacity
  def logs(state):
    if enable_logs:
      log = [coins_value, inserted_coins_qty, count(source_coins),
             limit_rules, empty_rules,
              current_funds_coins, vm_capacity , collected_coins,
              total_coins_in_spec,
              blacklisted_coins, chat_matrix, pass_coins_value ]
      
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:      
      return state
    
  def insert_funds_coins(new_coins, qty_to_dpl):
    'insert coins to the funds when is not empty new coins'
    def fn(old_coins):
      if is_empty(new_coins):
        return old_coins
      else:
        extract_coins  = first(new_coins)
        new_coins_dupl = dupl_list(extract_coins, qty_to_dpl)
        return concat(old_coins, new_coins_dupl)
    return fn

  
  def update_funds_amounts(collected_funds):
    def fn(old_amounts):
      return sum(collected_funds) 
    return fn

  
  def calculate_funds(state):
    collected_funds = get_in(vm_funds_coins, state)
    return update_in(state, vm_funds_amounts, update_funds_amounts(collected_funds))
  
  return  thread_first(
    state,
    (update_in, vm_funds_coins, insert_funds_coins(pass_coins_value, inserted_coins_qty)),
    (calculate_funds),
    (logs) # ultility to inspect state 
  )

```   

**3.2 Util Insert Coins Functions**    


```python 
def total_coins_in_list(coins_list):
  'return sum number qty of the coins'
  return reduce(lambda acc,x: acc + get_qty(x), coins_list , 0)

@curry
def is_coins_stock_available(limit_rules, current_coins):
  'return boolean true false when given coins_value is out of stock from its limit rules'
  'making sure coins qty is not zero and its not bellow limit '
  coins_qty   = int(get_qty(current_coins))  # 100
  coins_value = get_value(current_coins) #10
  limit       = get_limit(coins_value, limit_rules)
  return (coins_qty > 0) and coins_qty >= limit

@curry
def find_coins_is_available(coins_limit_rules, coins_list):
  'return filtered array or empty array, out of stock coins given coins limit rules and more than zero, compare by its value , qty > 0, qty > rules'
  return list(filter(is_coins_stock_available(coins_limit_rules), coins_list))

@curry
def is_coins_stock_not_available(limit_rules, current_coins):
  'not available coins check if qty is not valid'
  coins_qty   = int(get_qty(current_coins))  # 100
  coins_value = get_value(current_coins) #10
  limit       = get_limit(coins_value, limit_rules)
  return (coins_qty <= 0) or (coins_qty <= limit)

@curry 
def find_coins_is_not_available(limit_rules, coins_list):
  'get all value coins not available coins, return only the qty below zero and limits'
  return pipe(coins_list, filter(is_coins_stock_not_available(limit_rules)), list)

@curry 
def get_coins_is_not_available(limit_rules, coins_list):
  'get all value coins not available coins, return only the qty below zero and limits'
  return pipe(coins_list, filter(is_coins_stock_not_available(limit_rules)), maps_value, list)

@curry
def get_blacklisted_coins(limit_rules, empty_rules, source_coins):
  'get all blacklisted coins by comparing it with empty rules'
  def reduce_fn(acc,y):
    v = get(str(y), empty_rules, [])
    if bool(v):
      return concat(acc, v)
    else:
      return acc
    
  return reduce(
    reduce_fn,
    get_coins_is_not_available(limit_rules, source_coins), [])

@curry
def find_coins_is_not_blacklisted(blacklisted, coins_list):
  'check if coins already blacklisted, return its empty array if blacklisted'
  return list(
    filter(
      lambda x: not is_contains(x['value'], blacklisted),
      coins_list))

@curry
def is_coins_predicted_qty_available(inserted_qty, vm_capacity, x):
  'return true if predicted stock is available, or intended inserted qty is more then the reach'
  coins_qty = get_qty(x)
  limit = vm_capacity
  # actually find better ways, is not trully falid but is ok, because thereis other qty coins 
  predicted_qty = coins_qty + inserted_qty
  passd = limit >= max(1, predicted_qty)
  return passd
  
@curry
def find_coins_by_predicted_qty(inserted_qty, vm_capacity, coins_list):
  'filter all possibility qty predicted, coins should more than zero, not meet by rules, qty predicted available in the storage, pass the current coins or return empty'  
  return list(filter(is_coins_predicted_qty_available(inserted_qty, vm_capacity),coins_list))


def is_accepting_more_coins(intended_qty, collected_coins, all_coins_qty, vm_capacity): 
  'return true if collected_coins in the deposit is not more than accepted coins, also the total coins on the spec quantity'
  return count(collected_coins) + all_coins_qty + intended_qty <= vm_capacity

@curry
def find_coins_capacity_check(intended_qty, collected_coins, all_coins_qty, vm_capacity, current_coins):
  'used by curry it should return filtered is accepting more coins by querying back the state'
  pass
  
def find_coins_by_filter(filter_fn, coins_list):
  ' filter composition fn , flexible finding coins availibility, predicted qty, is vm_accepting more input, even more'
  return list(filter(filter_fn, coins_list))

def is_contains(keys, seqs):
  'return boolean if keys or value in the elements of sequences array or dictionary'
  return keys in seqs

@curry
def find_coins_by(key, value ,coins_list):
  'find coins by its key value'
  return list(filter(lambda x: get(key, x, 0) == value, coins_list))

@curry
def find_coins_by_alias(value, coins_list):
  'find coins by its alias'
  return list(filter(lambda x: is_contains(value, x['alias']), coins_list))

```   


**3.3 Test inserting coins**    


```python 
def test_inserting_coins():
  'test coins insertion is valid'  
  'Notes: You cannot Collect more coins if in the collected coins size > vm_capacity + current_coins'
  
  coins_defisit100 = [
    {'value':10, 'qty':10,},
    {'value':50, 'qty':0,},
    {'value':100, 'qty':4,},
    {'value':500 , 'qty': 100}]
  #all_qty = 113
  state = assign_initial_spec(coins_defisit100)
  
  inserted_500_1 = thread_first(insert_coins(500,1))
  inserted_100_1 = thread_first(insert_coins(100, 1))
  inserted_50_1  = thread_first(insert_coins(50, 1))
  inserted_100_20 = thread_first(insert_coins(100,20)) 
  inserted_100_4989 = thread_first(insert_coins(100, 4989)) #near full
  inserted_100_5001 = thread_first(insert_coins(100, 5001))
  return inserted_100_1(state)['vm']['funds_coins']

  assert inserted_500_1(state)['vm']['funds_coins'] == [],'Should blacklisted '
  assert inserted_100_1(state)['vm']['funds_coins'] == [100],'Should available'
  assert inserted_50_1(state)['vm']['funds_coins'] == [], 'Not available caused current qty == 0, in order vm to work it should added at least one to read, it strange but ya keep going on'
  assert count(inserted_100_20(state)['vm']['funds_coins']) == 20, 'Should added to the vm 20 coins of 100'
  assert inserted_100_20(state)['vm']['funds_amounts'] == 2000 ,' added sum to the calculated funds'
  assert inserted_100_5001(state)['vm']['funds_amounts'] == 0 ,'Not added caused by over vm_capacity'
  assert inserted_100_4989(state)['vm']['funds_amounts'] == 0 ,'Not added caused by over vm_capacity'
def test_is_coins_stock_available():
  val = 100,
  qty = 1
  coins = {'value':100, 'qty':10,}
  coins0 = {'value':100, 'qty':0,}
  coins3 = {'value':100 , 'qty': 0}
  assert is_coins_stock_available(limit_rules, coins) == True, 'True, should passed qty'
  assert is_coins_stock_available(limit_rules, coins0) == False, 'zero qty'
  assert is_coins_stock_available(limit_rules, coins3) == False, 'meet by limit'

def test_is_coins_stock_not_available():
  val = 100,
  qty = 1
  coins = {'value':100, 'qty':10,}
  coins0 = {'value':100, 'qty':0,}
  coins3 = {'value':100 , 'qty': 0}
  assert is_coins_stock_not_available(limit_rules, coins)  == False
  assert is_coins_stock_not_available(limit_rules, coins0) == True
  assert is_coins_stock_not_available(limit_rules, coins3) == True
  
def test_get_coins_is_not_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]  
  assert get_coins_is_not_available(limit_rules, coins_list) == [100, 500], 'Should 100 caused by limiter, 500 caused by qty is zero'

def test_find_coins_is_not_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]
  assert count(find_coins_is_not_available(limit_rules, coins_list)) == 2
  
test_find_coins_is_not_available()

def test_find_coins_is_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':2,},
    {'value':500 , 'qty': 0}]
  assert find_coins_is_available(limit_rules, coins_list)[0]['value'] == 10

  
def test_get_blacklisted_coins():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]
  
  coins_list_10_defisit = [
    {'value':10, 'qty':8,},
    {'value':100, 'qty':100,},
    {'value':500 , 'qty': 100}]

  assert get_blacklisted_coins(limit_rules, empty_rules, coins_list) == [500], 'limited 100 then 500 should not available'
  assert get_blacklisted_coins(limit_rules, empty_rules, coins_list_10_defisit) == [10, 50, 100, 500], 'Should be all even 100 has the qty'


def test_is_coins_predicted_qty_available():
  intended_qty = 10
  vm_capacity  = 15
  coins_list10  = {'value':10, 'qty':4 } # 4
  coins_list15  = {'value':10, 'qty':6 }
  assert is_coins_predicted_qty_available(intended_qty, vm_capacity, coins_list10) == True 
  assert is_coins_predicted_qty_available(intended_qty, vm_capacity, coins_list15) == False 


def test_find_coins_by_predicted_qty():
  intended_qty = 10
  vm_capacity  = 15
  coins_list10  = [{'value':10, 'qty':4 }] # 4
  coins_list15  = [{'value':10, 'qty':6 }]
  assert count(find_coins_by_predicted_qty(intended_qty, vm_capacity, coins_list10)) == 1
  assert find_coins_by_predicted_qty(intended_qty, vm_capacity, coins_list15) == [], 'out of stock'
  

def test_is_accepting_more_coins():
  collected_coins = [10]*10
  collected_coins_full = [10]*190 
  intended_qty  = 12 
  all_coins_qty  = 100
  vm_capacity    = 200
  assert is_accepting_more_coins(intended_qty, collected_coins, all_coins_qty, vm_capacity) == True, '10 + 12 = 22 + 100  = 122 < 200'
  assert is_accepting_more_coins(intended_qty, collected_coins_full, all_coins_qty, vm_capacity) == False, 'Over vm Size, 190 + 12 = 202 == oversize vm_capacity'

```  

#### 4. Selecting items    
**4.1 Selecting items flow**   


> 1. Find selected items is match and exists in the spec state    
> 2. Check wheather is already selecting and items, if true then calculate next funds from the predicted funds amounts left, not the funds amounts first time inserted   
> 3. Vm should able to hold selection quantity, our vm only limited by 32 selection at one time    
> 4. The intended qty of selection should be predicted in stock, meaning it should available in items specs qty   
> 5. Calculated funds should be enough, qty * prices should less than  < current funds,    
>   
> Flow would be   
> Filter -> build chat matrix -> calculate predicted_funds -> update selected_items in the db  
>   
> Retun new transformed state     


**4.2 Main select item functions**    

```python
@curry
def select_items(items_selector, items_qty, state):
  'select items by its items_selector assign in to the selected_items state'
  source_items                  = get_in(spec_items, state)
  selected_items                = get_in(vm_selected_items, state)
  funds_amounts                 = get_in(vm_funds_amounts, state)
  predicted_funds               = get_in(vm_predicted_funds, state)
  choosen_items                 = find_items_by_selector(items_selector, source_items)
  selection_capability          = get_in(['spec', 'vm_descriptions', 'no_item_selection'], state)

  if is_already_selecting_items(selected_items):
    funds_left  = predicted_funds
  else:
    funds_left  = funds_amounts

  pass_vm_capable_holding_items = list(filter(is_capable_holding_items(selection_capability, items_qty), choosen_items))
  pass_out_of_stock             = list(filter(is_not_out_of_stock, pass_vm_capable_holding_items))
  pass_predicted_available      = list(filter(is_predicted_qty_available(items_qty), pass_out_of_stock ))
  pass_predicted_funds_enough   = list(filter(is_predicted_not_over_prices(funds_left, items_qty),  pass_predicted_available))

  map_pass_items = lambda x: {'number': x['number'], 'intended_qty': items_qty, 'prices': x['prices']}
  pass_items  = list(map(map_pass_items, pass_predicted_funds_enough))

  calculated_predicted_funds = reduce(lambda acc,x: acc - (x['intended_qty'] * x['prices']) , pass_items,  funds_left)
    
  chat_matrix = [
    int(is_empty(choosen_items)),
    int(is_empty(pass_vm_capable_holding_items)),
    int(is_empty(pass_out_of_stock)),
    int(is_empty(pass_predicted_available)),
    int(is_empty(pass_predicted_funds_enough))
  ]

  def logs(state):
    if enable_logs:
      log = [calculated_predicted_funds, pass_items, items_selector, items_qty, count(source_items),
             selected_items, funds_amounts, predicted_funds,
             choosen_items, selection_capability,funds_left, chat_matrix]
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:
      return state 
  
  
  def selected_already_exists(y, list_items):
    'find if it already exists in list items by comparing number'
    return list(filter(lambda x: x['number'] == y['number'], list_items))

  def update_member_items(y, list_items):
    'transform each member in items list into y if matched by number'
    def map_fn(x):
      if y['number'] == x['number']: return y
      else: return x
    return list(map(map_fn, list_items))
  
  def update_selected_items(new_items):
    'if new items already there, then update otherwise concat'
    def fn(old_items):
      if is_empty(new_items):
        return old_items
      else:
        unpack_items = first(new_items)
        s = selected_already_exists(unpack_items, old_items)
        if is_empty(s):
          return concat(old_items, [unpack_items])
        else:
          return update_member_items(unpack_items, old_items)
        
    return fn
  
  return thread_first(
    state,
    (update_in, vm_selected_items, update_selected_items(pass_items)),
    (assoc_in, vm_predicted_funds, calculated_predicted_funds),
    # predicted_funds update continously
    (logs)
  )
```   

**4.2 Utility selecting functions**      

```python 

@curry 
def find_items_by_number(items_number, items_list):
  return list(filter(lambda x: get_in(['number'], x , 0) == items_number, items_list ))

@curry 
def find_items_by_name(items_name, items_list):
  return list(filter(lambda x: get_in(['name'], x, '') == items_name, items_list))

def find_items_by_selector(items_selector, items_list):
  'find all items by given selector if its number then by number,if its string then by its name'
  #notes: name should unique, id should also unique
  
  if isinstance(items_selector, int):
    return find_items_by_number(items_selector, items_list)
	
  else:
    return find_items_by_name(items_selector, items_list)

def dupl_list(val, qty):
  'return array of duplicated value amunt of qty'
  m = max(1, qty)
  return [val] * qty


def is_not_out_of_stock(current_items):
  qty = get_qty(current_items)
  return qty > 0

@curry
def is_capable_holding_items(vm_selection_capability, predicted_qty, _):
  return vm_selection_capability >= predicted_qty

def is_already_selecting_items(selected_items):
  'is it already selecting items on previously interaction'
  return count(selected_items) > 0 

@curry 
def is_predicted_qty_available(predicted_qty, current_items):
  return predicted_qty <= get_qty(current_items)

@curry
def is_predicted_not_over_prices(funds_left, predicted_qty, current_items):
  prices = get_prices(current_items)
  return  le((prices * predicted_qty), funds_left)

```   

**4.3 Test selecting items**    

```python 
def test_select_items():
  # notes: cover up all the corner cases 
  items_list = create_items(
    [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
    [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
    [3, 'Chicki', 50, 600, {'tags':['snacks']}],
    [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
    [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
    [6, 'Sprite', 170, 0, {'tags':['canned drinks']}]) #out of stock 
  
  state = assign_initial_spec({}, items_list)
  
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  select_items_1_1 =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3)
  )

  'it should update the previous items if selected twices before purchase'
  assert select_items_1_1['vm']['selected_items'] ==  [{"number": 1,"intended_qty": 2,"prices": 120},{"number": 2,"intended_qty": 1,"prices": 100}]
  assert select_items_1_1['vm']['predicted_funds'] == 240

def test_find_items_by_name():
  selectors = 'Canned Coffee'
  assert count(find_items_by_name(selectors, items_list)) == 1
  
def test_find_items_by_number():
  selectorn = 1
  assert count(find_items_by_number(selectorn, items_list)) == 1

def test_find_items_by_selector():
  selectorn = 1
  selectors = 'Canned Coffee'
  assert count(find_items_by_selector(selectorn, items_list)) == 1
  assert count(find_items_by_selector(selectors, items_list)) == 1

def test_is_already_selecting_items():
  assert is_already_selecting_items([32]) == True

def test_is_not_out_of_stock():
  assert is_not_out_of_stock({'qty':0}) == False
  assert is_not_out_of_stock({'qty':1}) == True 

def test_is_capable_holding_items():
  'is vm capable holding n much items'
  predicted1= 32
  predicted2= 300
  capability = 32
  _ = ''
  assert is_capable_holding_items(capability, predicted1, _) == True 
  assert is_capable_holding_items(capability, predicted2, _) == False


def test_is_predicted_qty_available():
  'is inserted items in qty is available in the stock'
  intended_qty = 10
  intended_qty2 = 11
  items = {'qty':10}
  assert is_predicted_qty_available(intended_qty, items) == True
  assert is_predicted_qty_available(intended_qty2, items) == False

def test_is_predicted_not_over_prices():
  'test is prices * qty <= funds left'
  intended_qty = 1
  intended_qty2 = 10
  intended_qty3 = 2 
  items = {'qty':10, 'prices':100}
  funds_left = 120
  funds_left2 = 200
  funds_left3 = 10
  assert is_predicted_not_over_prices(funds_left, intended_qty, items) == True
  assert is_predicted_not_over_prices(funds_left3, intended_qty, items) == False, 'funds_left not enough'
  assert is_predicted_not_over_prices(funds_left2, intended_qty2, items) == False, 'Not enough qty and overprices'
  assert is_predicted_not_over_prices(funds_left2, intended_qty3, items) == True, 'still on the reach'
  
```   

#### 5. Purchasing   
> Whats going on here,    
>   
> 1. It should have a selected items in the vm state, check wheater it is already selecting an items before  
> 2. Take out the calculation of the predicted funds calculated by selecting items functions   
> 3. Recalculate quantity of the purchased items, deduct qty - intended-qty of the spec items list   
> 4. Recalculate funds-left in the vm state after purchase   
> 5. Update predicted funds to zero    
> 6. If there is a funds amounts left then we do this scenario   
> 6a. Funds <=0 : collect coins directly and reset inserted funds   
> 6b. Funds left more than < minimum prices of all items in the spec then we recommend random items   
>     Recommended items algorithm based on funds left + minimum prices + random index   
> 7. Update the return gate give the selected items to the retun gate   
>    


**5.1 Purchasing Functions**   

```python 

def purchase_items(state):
  'making purchase items on the list of selected_items state'
  selected_items  = get_in(vm_selected_items, state)
  predicted_funds = get_in(vm_predicted_funds, state)
  funds_coins     = get_in(vm_funds_coins, state)
  is_empty_socket = is_already_selecting_items(selected_items)
  source_items    = get_in(spec_items, state)
  min_prices      = get_minimum_prices(source_items)
  
  recalculate_qty        = update_last(spec_items, deduct_qty_items(selected_items))
  recalculate_funds      = update_last(vm_funds_amounts, calculate_funds(selected_items, predicted_funds, is_empty_socket))
  update_predicted_funds = assoc_last(vm_predicted_funds, 0)
  empty_selected_items   = assoc_last(vm_selected_items, [])
  collect_coins          = update_last(db_collected_coins, collect_all_coins(funds_coins))
  return_items_to_outlet = assoc_last(vm_outlet, selected_items)
      
  new_state = thread_first(
    state, 
    recalculate_qty,
    recalculate_funds,
    update_predicted_funds,
    return_items_to_outlet,
    empty_selected_items 
  )

  funds_left = get_in(vm_funds_amounts, new_state)
  
  def logs(state):
    if enable_logs:
      log = [selected_items, funds_left, is_empty_socket ]
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:
      return state

  if funds_left <= 0:
    reset_inserted_funds = assoc_last(vm_funds_amounts, 0)
    reset_funds_coins    = assoc_last(vm_funds_coins, [])
    
    new_state = thread_first(
      new_state,
      collect_coins,
      reset_inserted_funds,
      reset_funds_coins
    )
  elif funds_left >= min_prices:
    recommend_items = update_last(vm_recommend_items, rnd_recommend_items(source_items, funds_left ))
    new_state = thread_first(new_state, recommend_items)

  def update_return_gate(state):
    rr = get_in(vm_return_gate, state, [])
    if is_not_empty(rr):
      return assoc_in(state, vm_return_gate, [])
    else:
      return state 
  
  return thread_last(new_state,
                     update_return_gate,
                     logs) 
```  

**5.2 Utility purchasing items**   


```python
def calculate_funds(new_items, predicted_funds, is_empty_socket):
  def fn(old_value):
    if is_empty(new_items):
      return old_value
    else:
      return predicted_funds
    
  return fn

def deduct_qty_items(new_items):
  def map_fn(new_items, old_items):
    if old_items['number'] == new_items['number']:
      new_qty = old_items['qty'] - new_items['intended_qty']
      return assoc_in(old_items, ['qty'], new_qty)
    else:
      return old_items

  map_fn = curry(map_fn)
  
  def reduce_fn(acc, x):
    'x is new items where acc is old items'
    return list(map(map_fn(x), acc))
    
  def fn(old_items):
    if is_empty(new_items):
      return old_items
    else:
      transformed_old_items  = list(reduce(reduce_fn, new_items, old_items))
      return transformed_old_items 
  return fn

@curry
def update_last (location, fn, state):
  return update_in(state, location, fn)

@curry
def assoc_last(location, value, state):
  return assoc_in(state, location, value)

def collect_all_coins(all_coins):
  def fn(old_value):
    if is_empty(all_coins):
      return old_value
    else:
      return concat(old_value, all_coins)
  return fn


def get_minimum_prices(source_items):
  shorted_out = list(map(lambda x: x['prices'], source_items))
  return min(shorted_out)

def rnd_recommend_items(source_items, funds_left):
  'recommend random i tems return array [items]'
  def fn(old_value):
    if is_empty(source_items):
      return old_value
    else:
      filtered = list(filter(lambda x: funds_left >= x['prices'], source_items ))
      recommended = random.choice(filtered)
      return recommended 
  return fn

```

**5.3 Test the purchasing**


```python

def test_purchase_items():
  'test cases scenario when there are funds left of purchased items'
  state = assign_initial_spec()
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  select_items_funds_left =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3)
  )
  select_items_no_funds_left =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 4),
    (select_items, 2, 3)
  )
  purchased_items_no_funds =  thread_last(
    select_items_no_funds_left,
    (purchase_items)
  )
  purchased_items_funds_left = thread_last(
    select_items_funds_left,
    (purchase_items)
  )
  
  outlet = [
    {
        "number": 1,
        "intended_qty": 2,
        "prices": 120
    },
    {
        "number": 2,
        "intended_qty": 1,
        "prices": 100
    }
  ]
  funds_left = 240
  assert purchased_items_funds_left['db']['collected_coins'] == [],'Should not collect all coins yet'
  assert purchased_items_funds_left['vm']['funds_amounts'] == funds_left
  assert purchased_items_funds_left['vm']['outlet'] == outlet
  assert is_not_empty(purchased_items_funds_left) == True, 'Should gived recommendation'
  assert purchased_items_funds_left['vm']['predicted_funds'] == 0, 'reseted to zero'
  assert purchased_items_funds_left['vm']['selected_items'] == [], 'Should empty out after purchase'
  
  'No funds left then reset funds coins, funds_amounts, collect all coins'
  
  assert count(purchased_items_no_funds['vm']['outlet']) == 2
  #assert purchased_items_no_funds['vm']['funds_coins'] == []
  assert purchased_items_no_funds['vm']['funds_amounts'] == 0
  assert purchased_items_no_funds['db']['collected_coins'] == [500,100,100], 'All coins is collected'
  assert purchased_items_no_funds['vm']['selected_items'] == []
  assert purchased_items_no_funds['vm']['recommended_items'] == {},' Should not recommed the next items'
  return purchased_items_funds_left

def test_is_collected_amounts_equal():
  assert is_collected_amounts_eq([100, 100, 500], 600) == False
  assert is_collected_amounts_eq([100, 100, 500], 700) == True

def test_create_coins_exchange_matrix():
  exchange_rules = [100,10]
  amounts=240
  assert create_coins_exchange_matrix(exchange_rules, amounts)['used'] == [100, 100, 10, 10, 10, 10]

def test_deduct_coins_qty_freq():
  freq =  {100: 2, 10: 8}
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':50, 'qty':0,},
    {'value':100, 'qty':4,},
    {'value':500 , 'qty': 100}]
  deducted = deduct_coins_qty_freq(freq, coins_list)
  assert deducted[0]['qty'] == 2
  assert deducted[1]['qty'] == 0
  assert deducted[2]['qty'] == 2
  assert deducted[3]['qty'] == 100
```   


#### 6. Return coins 
> Return coins is the interesting things in all part of the vending model, we should return coins by rules not bundled algorithm into the system,   
> Then we also need to find out what was the optimize way to retun the coins, the system should able to find out what was the minimum number coins used by system       
> If it 240 funds amounts left then it should not return 10 x24 rather than use 100 x 2 and the rest 40 by 10    
   
> The state flow model    
> 1. Check weather if the last transaction is already purchased or not, we can get this by comparing sum inserted coins funds is should not equal than funds amounts left and predicted funds   
> 1a. If it is not purchased, then return coins used as it is, need to update qty of coins in the system determined by how much frequencies used by users    
> 1b. Build coins matrix vector in python dictionary calculated coins used, then need to update qty of the coins is determined by these aproach    
> 2. Collect all coins in the state to the db, take all collected coins used, update the db    
> 3. Deduct quantity of all coins used by frequently used coins, qty spec items - intended-qty   
> 4, Reset funds coins, predicted-funds, selected-items, and outlet   
> 5. Return the calculated retun-exchange to the outlet gate in the state->vm->return_gate   
>   
> Return new transformed state to be used to the next operation   

 
**6.1. Return coins Functions**   

```python

def return_coins(state):
  'return coins left when there is funds_amounts > 0 '
  source_coins     =  get_in(spec_coins, state)
  funds_coins       = get_in(vm_funds_coins, state)
  funds_amounts     = get_in(vm_funds_amounts, state)
  is_not_purchased  = is_collected_amounts_eq(funds_coins, funds_amounts)
  exchange_rules    = get_in(spec_coins_exchange_rules, state)
  coins_list        = maps_value(source_coins)

  if is_not_purchased:
    coins_used = funds_coins
    need_updated_coin_qty = frequencies(coins_used)
    #collect_coins = lambda state: state 
  else:
    coins_matrix          = create_coins_exchange_matrix(exchange_rules, funds_amounts)
    coins_used            = get('used', coins_matrix, [])
    need_updated_coin_qty = frequencies(coins_used)

  #print('coins to use', coins_used)
  
  collect_coins         = update_last(db_collected_coins, collect_all_coins(funds_coins))

  # thread operation
  deduct_qty            = update_last(spec_coins, deduct_coins_qty_freq(need_updated_coin_qty))
  reset_inserted_funds  = assoc_last(vm_funds_amounts, 0)
  reset_funds_coins     = assoc_last(vm_funds_coins, [])
  reset_predicted_funds = assoc_last(vm_predicted_funds, 0)
  reset_selected_items  = assoc_last(vm_selected_items, [])
  update_return_gate    = assoc_last(vm_return_gate, coins_used)
  
  new_state = thread_first(
    state,
    collect_coins,
    deduct_qty,
    update_return_gate,
    reset_inserted_funds,
    reset_funds_coins,
    reset_predicted_funds,
    reset_selected_items,
    update_outlet_gate,
    update_recommended_items
    #record transactions 
  )

  def logs(state):
    return state
  
  # before mapped coins should available matrix
  
  return thread_first(new_state,logs)
```
**6.2. Utility functions used by return coins**   

```python 

def is_collected_amounts_eq(collected, amounts):
  return sum(collected) == amounts

@curry 
def create_coins_exchange_matrix(exchange_rules, amounts ):
  'return matrix of the calculated amounts, coinsisted of used coins in the multiple vecotor'
  #Notes: find more efficient method
  matrix = dict(coins_used=dupl_list(0, amounts + 1),
                used=[],
                coins_count=dupl_list(0, amounts +1))
  for coin in range(amounts + 1):
    coin_counted = coin
    new_coin = 1
    for i in [c for c in exchange_rules if c <= coin]:
      if matrix['coins_count'][coin - i] + 1 < coin_counted:
        coin_counted = matrix['coins_count'][coin - i] + 1
        new_coin = i
    matrix['coins_count'][coin] = coin_counted
    matrix['coins_used'][coin]  = new_coin
  def assoc_coins_used(matrix, amounts):
    coin = amounts 
    while coin > 0:
      the_coin = matrix['coins_used'][coin]
      matrix['used'] = concat(matrix['used'],[the_coin])
      coin = coin - the_coin
  
  assoc_coins_used(matrix, amounts) 
  return matrix

@curry
def deduct_coins_qty_freq(frequencies_coins, source_coins):
  'mapped all coins in the source coins reduce qty by frequencies of coins in used'
  def map_fn(x):
    current_x = get_in(['value'], x, 0)
    current_x_qty = get_in(['qty'], x, 0)
    current_y = get(current_x, frequencies_coins, -1)
    if current_y == -1:
      return x
    else:
      #print('deducted coins', current_x_qty - current_y)
      return assoc_in(x, ['qty'], current_x_qty - current_y)
    
  if is_empty(frequencies_coins):
    return source_items
  else:
    return list(map(map_fn, source_coins))

  
def update_outlet_gate(state):
  rr = get_in(vm_outlet, state, [])
  if is_not_empty(rr):
    return assoc_in(state, vm_outlet, [])
  else:
    return state

def update_recommended_items(state):
  rr = get_in(vm_recommend_items, state, {})
  if is_not_empty(rr):
    return assoc_in(state, vm_recommend_items, {})
  else:
    return state 

```   
> Our return coins function based on builded multi vector hash table, to get coin_used we need count back the element of array substracted 
> By amounts of funds that left or used.    


**6.3. The test return coins**    

```python 
def test_return_coins():
  'test scenario againts unexpected behaviours'
  state = assign_initial_spec()
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  
  select_items_then_return =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3),
    (return_coins) # immediet return without purchase 
  )
  select_items_purchase_return_funds_left = thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3),
    (purchase_items),
    (return_coins)
  )

  continues_update_collected_coins = thread_last(
    select_items_purchase_return_funds_left,
    (insert_coins, 500,2),
    (insert_coins, 100,3),
    (select_items, 2, 5),
  )
  
  then_immediet_return = thread_last(
    continues_update_collected_coins,
    (return_coins)
  )
  
  then_make_purchase = thread_last(
    continues_update_collected_coins, 
    (purchase_items),
  )

  the_make_selected_up_to_the_0_funds = thread_last(
    continues_update_collected_coins,
    (select_items,2, 8),
    (purchase_items)
  )
  then_make_return_after_purchase = thread_last(
    continues_update_collected_coins,
    (purchase_items),
    (return_coins)
  )

  'no_inserted_amounts_coins_then_select_items'
  'no_inserted_amounts_coins_then_purchase'
  'no_inserted_amounts_coins_then_return_change'

  return then_make_return_after_purchase

```

#### 7. Maintenances, Refill, Restock, Vault/Withdraw and Deposit Balance,    
#### 8. Events System    
#### 9. Rendering, raw_input, Python Curses  
