from venbot_core import *
import pytest

def pretify(x):
  'side-effects to print dict in better indent'
  return json.dumps(x, indent=2)

def pp(x):
  'pretty print x'
  print(pretify(x))


def list_value_of(key, item_list):
  return pipe(item_list, map(lambda x: x[key]), list)

def list_value(item_list):
  return pipe(item_list, map(lambda x: x['value']), list)


@curry
def pick(whitelist, d):
  'pick only key in the list dict'
  return keyfilter(lambda k: k in whitelist, d)

@curry
def omit(blacklist, d):
  'remove keys on the blacklist dict '
  return keyfilter(lambda k: k not in blacklist, d)

def test_initial_state():
  'test creation of initial state' 
  inits  = initial_state()
  assert  list(inits.keys()) == ['spec', 'db', 'vm', ]


def test_coins():
  'test coins creation'
  coins_with_value = coins(10)
  coins_currency_jpy = coins(10, 'JPY')
  coins_options = coins(100, {'alias': ['dim', 'yen'], 'qty':0 })
  coins_signature = coins(10, 'JPY', {'qty':20, 'alias': 'ten'}, {'weight':'10g', 'diameter':'5mm', 'thickness': '3mm'})
  
  assert coins_with_value['value'] == 10, 'create coins with single value'
  assert coins_currency_jpy['currency'] == 'JPY', 'coins currency JPY'
  assert coins_options['qty'] == 0
  assert coins_signature['signature'] ==  {'weight': '10g', 'diameter': '5mm', 'thickness': '3mm'}

coins_list = create_coins([100, 'RP', 100, 0, ['cepe']],
                          [500, 'RP', 200, 0, ['gope']],
                          [1000, 'RP', 300, 0, ['serebu']],
                          [5000, 'RP', 500, 0, ['goceng']])

coins_list_given_signature = create_coins(
  [10, 'JPY', 10, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
  [50, 'JPY', 20, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
  [100, 'JPY', 9, {'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
  [500, 'JPY', 3, {'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])

limit_rules =  { '10': 9, '100': 3}

coins_list_yen  = create_coins(
    [10, 'JPY', 10, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
    [50, 'JPY', 0, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
    [100, 'JPY', 2,{'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
    [500, 'JPY', 10,{'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])

coins_list_yen2  = create_coins(
  [10, 'JPY', 10, 0, 0],
  [50, 'JPY', 0, 0, 0],
  [100, 'JPY', 3, 0, 0],
  [500, 'JPY', 10, 0, 0])


def test_create_coins():
  'test create quick multiple coins arguments'
  'check wheater the value of each coins is correct'
  assert list_value(coins_list) == [100, 500, 1000, 5000]
  assert list_value_of('signature', coins_list_given_signature)[0] ==  {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}
  


def test_coins_yen():
  'create coins yen array, it should return array of coins created'
  coins_list = coins_yen()
  assert coins_list[0]['value'] == 10, 'should be ten yen on the first element'
  assert coins_list[0]['currency'] == 'JPY', 'and the currencies should be JPY'


def test_spec_coins_yen():
  'should be assigned all coins yen'
  spec_       = spec_coins_yen({})
  empty_rules = spec_['spec']['coins_empty_rules']
  limit_rules = spec_['spec']['coins_limit_rules']
  
  assert len(spec_['spec']['coins']) > 0
  assert isinstance(empty_rules ,dict) == True , 'Should assigned empty rules'
  assert isinstance(limit_rules, dict) == True, 'Should assigned limit rules'


def test_find_coins_by():
  'test finds coins by'
  assert find_coins_by('value', 100, coins_list)[0]['value'] == 100

 
def test_is_cointains():
  'test is contains'
  assert is_contains('test', [10, 'test']) == True
  assert is_contains('cepe', coins_list[0]['alias']) == True


def test_find_coins_by_alias():
  'finding coins by its aliases'
  assert find_coins_by_alias('gope', coins_list)[0]['value'] == 500

  
def test_items():
  'test create items'
  items_no_name = items(1, 'Canned')
  items_prices  = items(2, 'Coffee', 1000)
  items_with_qty = items(3, 'Coffee', 1000, 10)
  assert items_no_name['name'] == 'Canned'
  assert items_no_name['number'] == 1
  assert items_no_name['prices'] == 0, 'should be zero prices if not given prices'
  assert items_prices['prices'] == 1000
  assert items_with_qty['qty'] == 10
  
  
items_list = create_items(
  [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
  [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
  [3, 'Chicki', 50, 600, {'tags':['snacks']}],
  [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
  [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
  [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  

def test_create_items():
  'test create multiple items at onces'
  assert items_list[0]['number'] == 1
  assert items_list[0]['name'] == 'Canned Coffee'

  
def test_items_spec_creation():
  'test items spec of our state'
  spc = describe_spec_items({'foo':'bar'})
  assert is_empty(spc['spec']['items']) == False


def test_initial_spec():
  'test if valid specs of the state or assigned crucial aspec of the state'
  inits = assign_initial_spec()
  assert is_not_empty(get_in(spec_coins, inits)) == True, 'should contains some keys and vals from given specs'
  assert is_not_empty(get_in(spec_items, inits)) == True
  assert is_not_empty(get_in(spec_coins_empty_rules, inits)) == True
  assert is_not_empty(get_in(spec_coins_limit_rules, inits)) == True




def test_description_of_vending_machine():
  'test its is available no_selection, coins_capacity, items_capacity'
  assert spec_vending_machine('')['no_item_selection'] == 32
  assert spec_vending_machine('')['coins_capacity'] == 5000
  assert spec_vending_machine('')['items_capacity'] == 562

  
coins_defisit = create_coins(
    [10, 'JPY', 100, 0, 0],
    [50, 'JPY', 0, 0, 0], #not available caused by empty 
    [100, 'JPY', 3, 0, 0], #defisit 100 , 500 will not availalbe
    [500, 'JPY', 10, 0, 0])


limit_rules = { '10': 9, '100': 3}
empty_rules = { '10': [10, 50, 100, 500],
                '100': [500] }

coins_defisit100 = create_coins(
     [10, 'JPY', 100, 0, 0],
     [50, 'JPY', 0, 0, 0], #not available caused by empty 
     [100, 'JPY', 3, 0, 0], #defisit 100 , 500 will not availalbe
     [500, 'JPY', 10, 0, 0])

def test_is_coins_stock_available():
  val = 100,
  qty = 1
  coins = {'value':100, 'qty':10,}
  coins0 = {'value':100, 'qty':0,}
  coins3 = {'value':100 , 'qty': 0}
  assert is_coins_stock_available(limit_rules, coins) == True, 'True, should passed qty'
  assert is_coins_stock_available(limit_rules, coins0) == False, 'zero qty'
  assert is_coins_stock_available(limit_rules, coins3) == False, 'meet by limit'

def test_is_coins_stock_not_available():
  val = 100,
  qty = 1
  coins = {'value':100, 'qty':10,}
  coins0 = {'value':100, 'qty':0,}
  coins3 = {'value':100 , 'qty': 0}
  assert is_coins_stock_not_available(limit_rules, coins)  == False
  assert is_coins_stock_not_available(limit_rules, coins0) == True
  assert is_coins_stock_not_available(limit_rules, coins3) == True
  
def test_get_coins_is_not_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]  
  assert get_coins_is_not_available(limit_rules, coins_list) == [100, 500], 'Should 100 caused by limiter, 500 caused by qty is zero'

def test_find_coins_is_not_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]
  assert count(find_coins_is_not_available(limit_rules, coins_list)) == 2
  
test_find_coins_is_not_available()

def test_find_coins_is_available():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':2,},
    {'value':500 , 'qty': 0}]
  assert find_coins_is_available(limit_rules, coins_list)[0]['value'] == 10

  
def test_get_blacklisted_coins():
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':100, 'qty':3,},
    {'value':500 , 'qty': 0}]
  
  coins_list_10_defisit = [
    {'value':10, 'qty':8,},
    {'value':100, 'qty':100,},
    {'value':500 , 'qty': 100}]

  assert get_blacklisted_coins(limit_rules, empty_rules, coins_list) == [500], 'limited 100 then 500 should not available'
  assert get_blacklisted_coins(limit_rules, empty_rules, coins_list_10_defisit) == [10, 50, 100, 500], 'Should be all even 100 has the qty'


# def test_find_coins_is_not_blacklisted():
#   coins_list100 = [
#     {'value':10, 'qty':10,},
#     {'value':100, 'qty':3,},
#     {'value':500 , 'qty': 0}]
  
#   coins_list_10_defisit = [
#     {'value':10, 'qty':8,},
#     {'value':100, 'qty':100,},
#     {'value':500 , 'qty': 100}]

#   blacklisted_all = find_coins_is_not_blacklisted(limit_rules, empty_rules, coins_list_10_defisit)
#   blacklist_500   = find_coins_is_not_blacklisted(limit_rules, empty_rules, coins_list100)

#   assert find_coins_is_not_blacklisted(blacklist_500, [{'value':100}]) == [{'value':100}]
#   assert find_coins_is_not_blacklisted(blacklist_500, [{'value':500}]) == []
#   assert find_coins_is_not_blacklisted(blacklisted_all, [{'value':50}]) == []
#   return f


def test_is_coins_predicted_qty_available():
  intended_qty = 10
  vm_capacity  = 15
  coins_list10  = {'value':10, 'qty':4 } # 4
  coins_list15  = {'value':10, 'qty':6 }
  assert is_coins_predicted_qty_available(intended_qty, vm_capacity, coins_list10) == True 
  assert is_coins_predicted_qty_available(intended_qty, vm_capacity, coins_list15) == False 


def test_find_coins_by_predicted_qty():
  intended_qty = 10
  vm_capacity  = 15
  coins_list10  = [{'value':10, 'qty':4 }] # 4
  coins_list15  = [{'value':10, 'qty':6 }]
  assert count(find_coins_by_predicted_qty(intended_qty, vm_capacity, coins_list10)) == 1
  assert find_coins_by_predicted_qty(intended_qty, vm_capacity, coins_list15) == [], 'out of stock'
  

def test_is_accepting_more_coins():
  collected_coins = [10]*10
  collected_coins_full = [10]*190 
  intended_qty  = 12 
  all_coins_qty  = 100
  vm_capacity    = 200
  assert is_accepting_more_coins(intended_qty, collected_coins, all_coins_qty, vm_capacity) == True, '10 + 12 = 22 + 100  = 122 < 200'
  assert is_accepting_more_coins(intended_qty, collected_coins_full, all_coins_qty, vm_capacity) == False, 'Over vm Size, 190 + 12 = 202 == oversize vm_capacity'
  

def test_inserting_coins():
  'test coins insertion is valid'  
  'BIG Notes: You cannot Collect more coins if in the collected coins size > vm_capacity + current_coins'
  
  coins_defisit100 = [
    {'value':10, 'qty':10,},
    {'value':50, 'qty':0,},
    {'value':100, 'qty':4,},
    {'value':500 , 'qty': 100}]
  #all_qty = 113
  state = assign_initial_spec(coins_defisit100)
  
  inserted_500_1 = thread_first(insert_coins(500,1))
  inserted_100_1 = thread_first(insert_coins(100, 1))
  inserted_50_1  = thread_first(insert_coins(50, 1))
  inserted_100_20 = thread_first(insert_coins(100,20)) 
  inserted_100_4989 = thread_first(insert_coins(100, 4989)) #near full
  inserted_100_5001 = thread_first(insert_coins(100, 5001))
  return inserted_100_1(state)['vm']['funds_coins']

  assert inserted_500_1(state)['vm']['funds_coins'] == [],'Should blacklisted '
  assert inserted_100_1(state)['vm']['funds_coins'] == [100],'Should available'
  assert inserted_50_1(state)['vm']['funds_coins'] == [], 'Not available caused current qty == 0, in order vm to work it should added at least one to read, it strange but ya keep going on'
  assert count(inserted_100_20(state)['vm']['funds_coins']) == 20, 'Should added to the vm 20 coins of 100'
  assert inserted_100_20(state)['vm']['funds_amounts'] == 2000 ,' added sum to the calculated funds'
  assert inserted_100_5001(state)['vm']['funds_amounts'] == 0 ,'Not added caused by over vm_capacity'
  assert inserted_100_4989(state)['vm']['funds_amounts'] == 0 ,'Not added caused by over vm_capacity'
   
#test_inserting_coins()

def test_find_items_by_name():
  selectors = 'Canned Coffee'
  assert count(find_items_by_name(selectors, items_list)) == 1

  
def test_find_items_by_number():
  selectorn = 1
  assert count(find_items_by_number(selectorn, items_list)) == 1

def test_find_items_by_selector():
  selectorn = 1
  selectors = 'Canned Coffee'
  assert count(find_items_by_selector(selectorn, items_list)) == 1
  assert count(find_items_by_selector(selectors, items_list)) == 1


def test_is_already_selecting_items():
  assert is_already_selecting_items([32]) == True

def test_is_not_out_of_stock():
  assert is_not_out_of_stock({'qty':0}) == False
  assert is_not_out_of_stock({'qty':1}) == True 

def test_is_capable_holding_items():
  'is vm capable holding n much items'
  predicted1= 32
  predicted2= 300
  capability = 32
  _ = ''
  assert is_capable_holding_items(capability, predicted1, _) == True 
  assert is_capable_holding_items(capability, predicted2, _) == False


  
def test_is_predicted_qty_available():
  'is inserted items in qty is available in the stock'
  intended_qty = 10
  intended_qty2 = 11
  items = {'qty':10}
  assert is_predicted_qty_available(intended_qty, items) == True
  assert is_predicted_qty_available(intended_qty2, items) == False


def test_is_predicted_not_over_prices():
  'test is prices * qty <= funds left'
  intended_qty = 1
  intended_qty2 = 10
  intended_qty3 = 2 
  items = {'qty':10, 'prices':100}
  funds_left = 120
  funds_left2 = 200
  funds_left3 = 10
  assert is_predicted_not_over_prices(funds_left, intended_qty, items) == True
  assert is_predicted_not_over_prices(funds_left3, intended_qty, items) == False, 'funds_left not enough'
  assert is_predicted_not_over_prices(funds_left2, intended_qty2, items) == False, 'Not enough qty and overprices'
  assert is_predicted_not_over_prices(funds_left2, intended_qty3, items) == True, 'still on the reach'
  

def test_select_items():
  # notes: cover up all the corner cases 
  items_list = create_items(
    [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
    [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
    [3, 'Chicki', 50, 600, {'tags':['snacks']}],
    [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
    [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
    [6, 'Sprite', 170, 0, {'tags':['canned drinks']}]) #out of stock 
  
  state = assign_initial_spec({}, items_list)
  
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  select_items_1_1 =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3)
  )

  'it should update the previous items if selected twices before purchase'
  assert select_items_1_1['vm']['selected_items'] ==  [{"number": 1,"intended_qty": 2,"prices": 120},{"number": 2,"intended_qty": 1,"prices": 100}]
  assert select_items_1_1['vm']['predicted_funds'] == 240



def test_purchase_items():
  'test cases scenario when there are funds left of purchased items'
  state = assign_initial_spec()
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  select_items_funds_left =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3)
  )
  select_items_no_funds_left =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 4),
    (select_items, 2, 3)
  )
  purchased_items_no_funds =  thread_last(
    select_items_no_funds_left,
    (purchase_items)
  )
  purchased_items_funds_left = thread_last(
    select_items_funds_left,
    (purchase_items)
  )
  
  outlet = [
    {
        "number": 1,
        "intended_qty": 2,
        "prices": 120
    },
    {
        "number": 2,
        "intended_qty": 1,
        "prices": 100
    }
  ]
  funds_left = 240
  assert purchased_items_funds_left['db']['collected_coins'] == [],'Should not collect all coins yet'
  assert purchased_items_funds_left['vm']['funds_amounts'] == funds_left
  assert purchased_items_funds_left['vm']['outlet'] == outlet
  assert is_not_empty(purchased_items_funds_left) == True, 'Should gived recommendation'
  assert purchased_items_funds_left['vm']['predicted_funds'] == 0, 'reseted to zero'
  assert purchased_items_funds_left['vm']['selected_items'] == [], 'Should empty out after purchase'
  
  'No funds left then reset funds coins, funds_amounts, collect all coins'
  
  assert count(purchased_items_no_funds['vm']['outlet']) == 2
  #assert purchased_items_no_funds['vm']['funds_coins'] == []
  assert purchased_items_no_funds['vm']['funds_amounts'] == 0
  assert purchased_items_no_funds['db']['collected_coins'] == [500,100,100], 'All coins is collected'
  assert purchased_items_no_funds['vm']['selected_items'] == []
  assert purchased_items_no_funds['vm']['recommended_items'] == {},' Should not recommed the next items'
  return purchased_items_funds_left

def test_is_collected_amounts_equal():
  assert is_collected_amounts_eq([100, 100, 500], 600) == False
  assert is_collected_amounts_eq([100, 100, 500], 700) == True

def test_create_coins_exchange_matrix():
  exchange_rules = [100,10]
  amounts=240
  assert create_coins_exchange_matrix(exchange_rules, amounts)['used'] == [100, 100, 10, 10, 10, 10]

def test_deduct_coins_qty_freq():
  freq =  {100: 2, 10: 8}
  coins_list = [
    {'value':10, 'qty':10,},
    {'value':50, 'qty':0,},
    {'value':100, 'qty':4,},
    {'value':500 , 'qty': 100}]
  deducted = deduct_coins_qty_freq(freq, coins_list)
  assert deducted[0]['qty'] == 2
  assert deducted[1]['qty'] == 0
  assert deducted[2]['qty'] == 2
  assert deducted[3]['qty'] == 100


def test_return_coins():
  'scenario againts unexpected behaviours'
  state = assign_initial_spec()
  inserted_700 = thread_last(
    state,
    (insert_coins, 500,1),
    (insert_coins, 100,2)
  )
  
  select_items_then_return =  thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3),
    (return_coins) # immediet return without purchase 
  )
  select_items_purchase_return_funds_left = thread_last(
    inserted_700,
    (select_items, 1, 1),
    (select_items, 2, 1),
    (select_items, 1, 2),
    (select_items, 2, 3),
    (purchase_items),
    (return_coins)
  )

  continues_update_collected_coins = thread_last(
    select_items_purchase_return_funds_left,
    (insert_coins, 500,2),
    (insert_coins, 100,3),
    (select_items, 2, 5),
  )
  
  then_immediet_return = thread_last(
    continues_update_collected_coins,
    (return_coins)
  )
  
  then_make_purchase = thread_last(
    continues_update_collected_coins, 
    (purchase_items),
  )

  the_make_selected_up_to_the_0_funds = thread_last(
    continues_update_collected_coins,
    (select_items,2, 8),
    (purchase_items)
  )
  then_make_return_after_purchase = thread_last(
    continues_update_collected_coins,
    (purchase_items),
    (return_coins)
  )

  'no_inserted_amounts_coins_then_select_items'
  'no_inserted_amounts_coins_then_purchase'
  'no_inserted_amounts_coins_then_return_change'

  return then_make_return_after_purchase

#enable_logs                = False

#pp(test_return_coins())

