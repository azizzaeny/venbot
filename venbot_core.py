from toolz.curried import *
from operator import *
import uuid 
import json 
import time
import uuid
import random
import math


# =====================================================================
# 1. model state 
# =====================================================================


def initial_state():
  'create initial state of the vending consisting of spec, db, vm'
  return {
    'spec':{
      'coins':[],
      'items':[],
      'coins_empty_rules':{},
      'coins_limit_rules':{},
      'coins_exchange_rules':[],
      'commands':{},
      'vm_descriptions':{}
    },
    'db': {
      'events':[],
      'transactions':[],
      'stats': [],
      'chats': [],
      'collected_coins':[],
      'logs':[['initiated']]
    },
    'vm': {
      'maintenance': False,
      'funds_coins':[],
      'funds_amounts':0,
      'predicted_funds': 0,
      'locked': False,
      'selected_items': [],
      'recommended_items':{},
      'return_gate':[], #for coins 
      'outlet': [],     #for items purchased last 
      'iteraction_time':0,
      'started_time':0,
    }
  }

enable_logs                = False
spec_coins                 = ['spec', 'coins']
spec_items                 = ['spec','items']
spec_vm_description        = ['spec','vm_descriptions']
spec_coins_capacity        = ['spec','vm_descriptions', 'coins_capacity']
spec_coins_empty_rules     = ['spec','coins_empty_rules']
spec_coins_limit_rules     = ['spec','coins_limit_rules']
spec_coins_exchange_rules  = ['spec','coins_exchange_rules']
spec_commands              = ['spec','commands']
db_chat                    = ['db','chats']
db_collected_coins         = ['db', 'collected_coins']
db_logs                    = ['db', 'logs']
vm_funds_coins             = ['vm', 'funds_coins']
vm_funds_amounts           = ['vm', 'funds_amounts']
vm_selected_items          = ['vm', 'selected_items']
vm_predicted_funds         = ['vm', 'predicted_funds']
vm_funds_amounts           = ['vm', 'funds_amounts']
vm_recommend_items         = ['vm', 'recommended_items']
vm_outlet                  = ['vm', 'outlet']
vm_return_gate             = ['vm', 'return_gate']

# ============================================================
# 2. coins
# ============================================================

def guid():
  'return uuid string str(uuid.uuid4())'
  return uuid.uuid4().hex


def coins(value, currency='Rp', options={'alias':[], 'qty':0, }, signature={'weight':0, 'diameter':0, 'thickness':0}):
  'create coins with given value with currency and empty quantity'
  return merge({
    'id' : guid(),
    'value': value,
    'currency': currency
  }, options, {'signature': signature})



def create_coins(*coins_list_val_curr_qty_limt):
  'quick create coins with given of currency the rest arguments list is the coins value'
  def map_coins(i):
    [value, currency, qty, signature, alias] = i
    if signature == 0:
      signature = {'weight':0, 'diameter':0, 'thickness':0}
    if alias == 0:
      alias = []
    return coins(value, currency, {'qty': qty, 'alias':alias}, signature)
  
  return list(map(map_coins, coins_list_val_curr_qty_limt))


def coins_yen():
 'create coins yens with given array tupple list'
 coins_list  = create_coins(
   [10, 'JPY', 100, {'weight': '4.5g', 'thickness': '1.5mm', 'diameter': '23.5mm'}, ['ten']],
   [50, 'JPY', 200, {'weight': '4g', 'diameter': '21mm', 'hole': '4mm', 'shape': 'circular'}, ['fifty']],
   [100, 'JPY', 300,{'weight': '4.8g', 'diameter': '22.6mm', 'shape': 'circular'}, ['hundred']],
   [500, 'JPY', 500,{'weight': '7g', 'diameter': '26.5mm', 'thickness': '2mm'}, ['five hundred']])
 return coins_list


def spec_coins_yen(state={}):
  'create coins yen spec with limit and rules'
  coins_list            = coins_yen()
  empty_coins_yen_rules = { '10': [10, 50, 100, 500],
                            '100': [500] }
  limit_coins_yen_rules = { '10': 9, '100': 4}
  return thread_first(
    state,
    (assoc_in, spec_coins, coins_list),
    (assoc_in, spec_coins_empty_rules, empty_coins_yen_rules),
    (assoc_in, spec_coins_limit_rules, limit_coins_yen_rules))
  

# ============================================================
# 3. items 
# ============================================================


def items(no, name, prices=0, qty=0, options={'tags': [], 'alias': []}):
  'create items with given name prices and options'
  return merge({
    'id': guid(),
    'name': name,
    'number': no, 
    'prices': prices,
    'qty': qty
  }, options)


def create_items(*list_name_prices_qty):
  'quick create multiple of items'
  def map_items(i):
    [no, name, prices, qty, options] = i
    if options == 0:
      options = {'tags': [], 'alias':[] }      
    return items(no, name, prices, qty, options)

  return list(map(map_items, list_name_prices_qty))


def describe_spec_items(state={}):
  'create specs items for our vending machine'
  items_list = create_items(
    [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
    [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
    [3, 'Chicki', 50, 600, {'tags':['snacks']}],
    [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
    [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
    [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  
  return assoc_in(state, spec_items, items_list)


def spec_vending_machine(location=''):
  'describe the spec used by system' 
  'note: only used for items capacity and coins_capacity] the rest information not used'
  #'suppose information to be used on the extracted machine learning for predictive vm behaviours'
  return {
    'location_address': location,
    'model_number': 'FGG1XXMCY8',
    'coins_capacity': 5000,
    'items_capacity': 562,
    'no_item_selection': 32,
    'output_temperature':'0-23c',
    'voltage_power': '220-240v/50hz',
    'number_rack_col': 18,
    'dimension':'H 1834 * W 882 * D 852 mm',
    'weight': '197kg',
    'supported_payment': ['Coin bill'], #creditcard,
    'interaction_mode': ['button','chatbots app']}


def is_empty(l):
  return len(l) <= 0


def is_not_empty(l):
   return not is_empty(l)


def assign_initial_spec(coins_sp={}, items_sp={}, state ={}):
  'quick assign initial spec to the state, make the system ready to accept coins coins rules and items'
  if is_empty(state):
    state = initial_state() 
  if is_empty(coins_sp):
    coins_list   = coins_yen()
  else:
    coins_list   = coins_sp 

  empty_coins_yen_rules = { '10': [10, 50, 100, 500], '100': [500] }
  limit_coins_yen_rules = { '10': 9, '100': 4} 
  exchanges_rules = [100, 10]
  
  if is_empty(items_sp):
    items_list = create_items(
      [1, 'Canned Coffee', 120, 800, {'tags':['canned drinks']}],
      [2, 'Water PET Bottle', 100, 1000, {'tags':['bottle drinks']}],
      [3, 'Chicki', 50, 600, {'tags':['snacks']}],
      [4, 'Coca Colla', 170, 700, {'tags': ['canned drinks']}],
      [5, 'SPort drinks', 150, 500, {'tags': ['bottle drinks']}],
      [6, 'Sprite', 170, 0, {'tags':['canned drinks']}])
  else:
    items_list = items_sp
    
  value_spec_vending_machine = spec_vending_machine()
  return thread_first(
    state,
    (assoc_in, spec_coins, coins_list),
    (assoc_in, spec_coins_empty_rules, empty_coins_yen_rules),
    (assoc_in, spec_coins_limit_rules, limit_coins_yen_rules),
    (assoc_in, spec_items, items_list),
    (assoc_in, spec_coins_exchange_rules, exchanges_rules),
    (assoc_in, spec_vm_description, value_spec_vending_machine))
  

def is_contains(keys, seqs):
  'return boolean if keys or value in the elements of sequences array or dictionary'
  return keys in seqs

@curry
def find_coins_by(key, value ,coins_list):
  'find coins by its key value'
  return list(filter(lambda x: get(key, x, 0) == value, coins_list))

@curry
def find_coins_by_alias(value, coins_list):
  'find coins by its alias'
  return list(filter(lambda x: is_contains(value, x['alias']), coins_list))


def find_coins_by_signature():
  'real vending machine will find its coins type by its signature weight, size etc.'
  pass

def get_value(x):
  return get('value',x, 0)

def get_qty(x):
  return get('qty', x, 0)

def get_name(x):
  return get('name', x, '')

def get_number(x):
  return get('number', x, 0)

def get_prices(x):
  return get_in(['prices'], x, 0)


def maps_value(l):
  return list(map(lambda x: get_value(x), l))

def maps_name(l):
  return list(map(lambda x: get_names(x), l))

def maps_number(l):
  return list(map(lambda x: get_number(x), l))

def get_limit(x, limit_rules):
  return get(str(x), limit_rules, -1)




@curry
def is_coins_stock_available(limit_rules, current_coins):
  'return boolean true false when given coins_value is out of stock from its limit rules'
  'making sure coins qty is not zero and its not bellow limit '
  coins_qty   = int(get_qty(current_coins))  # 100
  coins_value = get_value(current_coins) #10
  limit       = get_limit(coins_value, limit_rules)
  return (coins_qty > 0) and coins_qty >= limit

@curry
def find_coins_is_available(coins_limit_rules, coins_list):
  'return filtered array or empty array, out of stock coins given coins limit rules and more than zero, compare by its value , qty > 0, qty > rules'
  return list(filter(is_coins_stock_available(coins_limit_rules), coins_list))



@curry
def is_coins_stock_not_available(limit_rules, current_coins):
  'not available coins check if qty is not valid'
  coins_qty   = int(get_qty(current_coins))  # 100
  coins_value = get_value(current_coins) #10
  limit       = get_limit(coins_value, limit_rules)
  return (coins_qty <= 0) or (coins_qty <= limit)

@curry 
def find_coins_is_not_available(limit_rules, coins_list):
  'get all value coins not available coins, return only the qty below zero and limits'
  return pipe(coins_list, filter(is_coins_stock_not_available(limit_rules)), list)

@curry 
def get_coins_is_not_available(limit_rules, coins_list):
  'get all value coins not available coins, return only the qty below zero and limits'
  return pipe(coins_list, filter(is_coins_stock_not_available(limit_rules)), maps_value, list)

@curry
def get_blacklisted_coins(limit_rules, empty_rules, source_coins):
  'get all blacklisted coins by comparing it with empty rules'
  def reduce_fn(acc,y):
    v = get(str(y), empty_rules, [])
    if bool(v):
      return concat(acc, v)
    else:
      return acc
    
  return reduce(
    reduce_fn,
    get_coins_is_not_available(limit_rules, source_coins), [])

@curry
def find_coins_is_not_blacklisted(blacklisted, coins_list):
  'check if coins already blacklisted, return its empty array if blacklisted'
  return list(
    filter(
      lambda x: not is_contains(x['value'], blacklisted),
      coins_list))






@curry
def is_coins_predicted_qty_available(inserted_qty, vm_capacity, x):
  'return true if predicted stock is available, or intended inserted qty is more then the reach'
  coins_qty = get_qty(x)
  limit = vm_capacity
  # actually find better ways, is not trully falid but is ok, because thereis other qty coins 
  predicted_qty = coins_qty + inserted_qty
  passd = limit >= max(1, predicted_qty)
  return passd


@curry
def find_coins_by_predicted_qty(inserted_qty, vm_capacity, coins_list):
  'filter all possibility qty predicted, coins should more than zero, not meet by rules, qty predicted available in the storage, pass the current coins or return empty'
  
  # coins_available = lambda x: and_(
  #   is_coins_stock_available(vm_capacity, x),
  #   is_coins_predicted_qty_available(inserted_qty, vm_capacity, x),)
  
  return list(filter(is_coins_predicted_qty_available(inserted_qty, vm_capacity),coins_list))






def is_accepting_more_coins(intended_qty, collected_coins, all_coins_qty, vm_capacity): 
  'return true if collected_coins in the deposit is not more than accepted coins, also the total coins on the spec quantity'
  return count(collected_coins) + all_coins_qty + intended_qty <= vm_capacity


@curry
def find_coins_capacity_check(intended_qty, collected_coins, all_coins_qty, vm_capacity, current_coins):
  'used by curry it should return filtered is accepting more coins by querying back the state'
  pass

  
def find_coins_by_filter(filter_fn, coins_list):
  ' filter composition fn , flexible finding coins availibility, predicted qty, is vm_accepting more input, even more'
  return list(filter(filter_fn, coins_list))



def total_coins_in_list(coins_list):
  'return sum number qty of the coins'
  return reduce(lambda acc,x: acc + get_qty(x), coins_list , 0)


@curry 
def find_items_by_number(items_number, items_list):
  return list(filter(lambda x: get_in(['number'], x , 0) == items_number, items_list ))

@curry 
def find_items_by_name(items_name, items_list):
  return list(filter(lambda x: get_in(['name'], x, '') == items_name, items_list))

def find_items_by_id():
  pass

def find_items_by_prices():
  pass

def find_items_by_eq_qty():
  pass

def find_items_similiar_tags():
  pass

def find_items_by_alias(items_selector, items_list):
  pass

def find_items_by_selector(items_selector, items_list):
  'find all items by given selector if its number then by number,if its string then by its name'
  #notes: name should unique, id should also unique
  
  if isinstance(items_selector, int):
    return find_items_by_number(items_selector, items_list)

  #elif isinstance(items_selector, list):
  #  return find_items_by_alias(items_selector, items_list)
  
  else:
    return find_items_by_name(items_selector, items_list)


#
# utility used ny items 
#
def dupl_list(val, qty):
  'return array of duplicated value amunt of qty'
  m = max(1, qty)
  return [val] * qty


def is_not_out_of_stock(current_items):
  qty = get_qty(current_items)
  return qty > 0

@curry
def is_capable_holding_items(vm_selection_capability, predicted_qty, _):
  return vm_selection_capability >= predicted_qty

def is_already_selecting_items(selected_items):
  'is it already selecting items on previously interaction'
  return count(selected_items) > 0 

@curry 
def is_predicted_qty_available(predicted_qty, current_items):
  return predicted_qty <= get_qty(current_items)

@curry
def is_predicted_not_over_prices(funds_left, predicted_qty, current_items):
  prices = get_prices(current_items)
  return  le((prices * predicted_qty), funds_left)

# ===========================================================
# 4. inserting coins 
# ===========================================================


@curry
def insert_coins(coins_value, inserted_coins_qty, state):
  'return new state with added new inserted coins to the spec_coins if all the check are valid'
  
  limit_rules                        = get_in(spec_coins_limit_rules, state, {})
  empty_rules                        = get_in(spec_coins_empty_rules, state, {})
  source_coins                       = get_in(spec_coins, state, [])
  current_funds_coins                = get_in(vm_funds_coins, state, [])
  vm_capacity                        = get_in(spec_coins_capacity, state, math.inf)
  collected_coins                    = get_in(db_collected_coins, state, [])
  
  total_coins_in_spec                = total_coins_in_list(source_coins)
  blacklisted_coins                  = get_blacklisted_coins(limit_rules, empty_rules, source_coins)

  pass_coins_by_value                  = find_coins_by('value', coins_value, source_coins)
  pass_coins_not_out_of_stock          = find_coins_is_available(limit_rules, pass_coins_by_value) #so we can make a changes 
  pass_coins_is_not_blacklisted        = find_coins_is_not_blacklisted(blacklisted_coins, pass_coins_not_out_of_stock)
  pass_coins_available_qty             = find_coins_by_predicted_qty(inserted_coins_qty, vm_capacity, pass_coins_is_not_blacklisted)
  pass_coins_capacity_check            = list(filter(lambda _: is_accepting_more_coins(inserted_coins_qty,collected_coins ,total_coins_in_spec, vm_capacity),
                                                     pass_coins_available_qty))
  
  #intended_qty, collected_coins, all_coins_qty, vm_capacity
  pass_coins_value                     = maps_value(pass_coins_capacity_check)

  chat_matrix                          = [
    int(is_empty(pass_coins_by_value)),
    int(is_empty(pass_coins_not_out_of_stock)),
    int(is_empty(pass_coins_is_not_blacklisted)),
    int(is_empty(pass_coins_available_qty)),
    int(is_empty(pass_coins_capacity_check)),
  ]
  #intended_qty, collected_coins, all_coins_qty, vm_capacity
  def logs(state):
    if enable_logs:
      log = [coins_value, inserted_coins_qty, count(source_coins),
             limit_rules, empty_rules,
              current_funds_coins, vm_capacity , collected_coins,
              total_coins_in_spec,
              blacklisted_coins, chat_matrix, pass_coins_value ]
      
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:      
      return state
    
  def insert_funds_coins(new_coins, qty_to_dpl):
    'insert coins to the funds when is not empty new coins'
    def fn(old_coins):
      if is_empty(new_coins):
        return old_coins
      else:
        extract_coins  = first(new_coins)
        new_coins_dupl = dupl_list(extract_coins, qty_to_dpl)
        return concat(old_coins, new_coins_dupl)
    return fn

  
  def update_funds_amounts(collected_funds):
    def fn(old_amounts):
      return sum(collected_funds) 
    return fn

  
  def calculate_funds(state):
    collected_funds = get_in(vm_funds_coins, state)
    return update_in(state, vm_funds_amounts, update_funds_amounts(collected_funds))
  
  return  thread_first(
    state,
    (update_in, vm_funds_coins, insert_funds_coins(pass_coins_value, inserted_coins_qty)),
    (calculate_funds),
    # notes: chat_matrix
    (logs) # ultility to inspect state 
  )


# ===========================================================
# 5. selecting items 
# ===========================================================

@curry
def select_items(items_selector, items_qty, state):
  'select items by its items_selector assign in to the selected_items state'
  source_items                  = get_in(spec_items, state)
  selected_items                = get_in(vm_selected_items, state)
  funds_amounts                 = get_in(vm_funds_amounts, state)
  predicted_funds               = get_in(vm_predicted_funds, state)
  choosen_items                 = find_items_by_selector(items_selector, source_items)
  selection_capability          = get_in(['spec', 'vm_descriptions', 'no_item_selection'], state)

  if is_already_selecting_items(selected_items):
    funds_left  = predicted_funds
  else:
    funds_left  = funds_amounts

  pass_vm_capable_holding_items = list(filter(is_capable_holding_items(selection_capability, items_qty), choosen_items))
  pass_out_of_stock             = list(filter(is_not_out_of_stock, pass_vm_capable_holding_items))
  pass_predicted_available      = list(filter(is_predicted_qty_available(items_qty), pass_out_of_stock ))
  pass_predicted_funds_enough   = list(filter(is_predicted_not_over_prices(funds_left, items_qty),  pass_predicted_available))

  map_pass_items = lambda x: {'number': x['number'], 'intended_qty': items_qty, 'prices': x['prices']}
  pass_items  = list(map(map_pass_items, pass_predicted_funds_enough))

  calculated_predicted_funds = reduce(lambda acc,x: acc - (x['intended_qty'] * x['prices']) , pass_items,  funds_left)
    
  chat_matrix = [
    int(is_empty(choosen_items)),
    int(is_empty(pass_vm_capable_holding_items)),
    int(is_empty(pass_out_of_stock)),
    int(is_empty(pass_predicted_available)),
    int(is_empty(pass_predicted_funds_enough))
  ]

  def logs(state):
    if enable_logs:
      log = [calculated_predicted_funds, pass_items, items_selector, items_qty, count(source_items),
             selected_items, funds_amounts, predicted_funds,
             choosen_items, selection_capability,funds_left, chat_matrix]
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:
      return state 
  
  
  def selected_already_exists(y, list_items):
    'find if it already exists in list items by comparing number'
    return list(filter(lambda x: x['number'] == y['number'], list_items))

  def update_member_items(y, list_items):
    'transform each member in items list into y if matched by number'
    def map_fn(x):
      if y['number'] == x['number']: return y
      else: return x
    return list(map(map_fn, list_items))
  
  def update_selected_items(new_items):
    'if new items already there, then update otherwise concat'
    def fn(old_items):
      if is_empty(new_items):
        return old_items
      else:
        unpack_items = first(new_items)
        s = selected_already_exists(unpack_items, old_items)
        if is_empty(s):
          return concat(old_items, [unpack_items])
        else:
          return update_member_items(unpack_items, old_items)
        
    return fn
  
  return thread_first(
    state,
    (update_in, vm_selected_items, update_selected_items(pass_items)),
    (assoc_in, vm_predicted_funds, calculated_predicted_funds),
    # predicted_funds update continously
    (logs)
  )
  






# ===========================================================
# 6a. making purchase 
# ===========================================================



def calculate_funds(new_items, predicted_funds, is_empty_socket):
  # def reduce_fn(acc,x):
  #   'x is new_items coming, acc old_value = 0 '
  #   if is_empty_socket:
  #     return acc
  #   else:
  #     new_funds =  predicted_funds
  #     return new_funds
    
  def fn(old_value):
    if is_empty(new_items):
      return old_value
    else:
      return predicted_funds
    
  return fn

def deduct_qty_items(new_items):
  def map_fn(new_items, old_items):
    if old_items['number'] == new_items['number']:
      new_qty = old_items['qty'] - new_items['intended_qty']
      return assoc_in(old_items, ['qty'], new_qty)
    else:
      return old_items

  map_fn = curry(map_fn)
  
  def reduce_fn(acc, x):
    'x is new items where acc is old items'
    return list(map(map_fn(x), acc))
    
  def fn(old_items):
    if is_empty(new_items):
      return old_items
    else:
      transformed_old_items  = list(reduce(reduce_fn, new_items, old_items))
      return transformed_old_items 
  return fn

@curry
def update_last (location, fn, state):
  return update_in(state, location, fn)

@curry
def assoc_last(location, value, state):
  return assoc_in(state, location, value)

def collect_all_coins(all_coins):
  def fn(old_value):
    if is_empty(all_coins):
      return old_value
    else:
      return concat(old_value, all_coins)
  return fn


def get_minimum_prices(source_items):
  shorted_out = list(map(lambda x: x['prices'], source_items))
  return min(shorted_out)

def rnd_recommend_items(source_items, funds_left):
  'recommend random i tems return array [items]'
  def fn(old_value):
    if is_empty(source_items):
      return old_value
    else:
      filtered = list(filter(lambda x: funds_left >= x['prices'], source_items ))
      recommended = random.choice(filtered)
      return recommended 
  return fn


def purchase_items(state):
  'making purchase items on the list of selected_items state'
  selected_items  = get_in(vm_selected_items, state)
  predicted_funds = get_in(vm_predicted_funds, state)
  funds_coins     = get_in(vm_funds_coins, state)
  is_empty_socket = is_already_selecting_items(selected_items)
  source_items    = get_in(spec_items, state)
  min_prices      = get_minimum_prices(source_items)
  
  recalculate_qty        = update_last(spec_items, deduct_qty_items(selected_items))
  recalculate_funds      = update_last(vm_funds_amounts, calculate_funds(selected_items, predicted_funds, is_empty_socket))
  update_predicted_funds = assoc_last(vm_predicted_funds, 0)
  empty_selected_items   = assoc_last(vm_selected_items, [])
  collect_coins          = update_last(db_collected_coins, collect_all_coins(funds_coins))
  return_items_to_outlet = assoc_last(vm_outlet, selected_items)
  
    
  new_state = thread_first(
    state, 
    recalculate_qty,
    recalculate_funds,
    update_predicted_funds,
    return_items_to_outlet,
    empty_selected_items 
  )

  funds_left = get_in(vm_funds_amounts, new_state)
  
  def logs(state):
    if enable_logs:
      log = [selected_items, funds_left, is_empty_socket ]
      return update_in(state, ['db','logs'], lambda acc: concat(acc, [log]))
    else:
      return state

  if funds_left <= 0:
    reset_inserted_funds = assoc_last(vm_funds_amounts, 0)
    reset_funds_coins    = assoc_last(vm_funds_coins, [])
    
    new_state = thread_first(
      new_state,
      collect_coins,
      reset_inserted_funds,
      reset_funds_coins
    )
  elif funds_left >= min_prices:
    recommend_items = update_last(vm_recommend_items, rnd_recommend_items(source_items, funds_left ))
    new_state = thread_first(new_state, recommend_items)

  def update_return_gate(state):
    rr = get_in(vm_return_gate, state, [])
    if is_not_empty(rr):
      return assoc_in(state, vm_return_gate, [])
    else:
      return state 
  
  return thread_last(new_state,
                     update_return_gate,
                     logs) 



# ===========================================================
# 6b. return coins 
# ==========================================================

def is_collected_amounts_eq(collected, amounts):
  return sum(collected) == amounts


@curry 
def create_coins_exchange_matrix(exchange_rules, amounts ):
  'return matrix of the calculated amounts, coinsisted of used coins in the multiple vecotor'
  #Notes: find more efficient method
  matrix = dict(coins_used=dupl_list(0, amounts + 1),
                used=[],
                coins_count=dupl_list(0, amounts +1))
  for coin in range(amounts + 1):
    coin_counted = coin
    new_coin = 1
    for i in [c for c in exchange_rules if c <= coin]:
      if matrix['coins_count'][coin - i] + 1 < coin_counted:
        coin_counted = matrix['coins_count'][coin - i] + 1
        new_coin = i
    matrix['coins_count'][coin] = coin_counted
    matrix['coins_used'][coin]  = new_coin
  def assoc_coins_used(matrix, amounts):
    coin = amounts 
    while coin > 0:
      the_coin = matrix['coins_used'][coin]
      matrix['used'] = concat(matrix['used'],[the_coin])
      coin = coin - the_coin
  
  assoc_coins_used(matrix, amounts) 
  return matrix

@curry
def deduct_coins_qty_freq(frequencies_coins, source_coins):
  'mapped all coins in the source coins reduce qty by frequencies of coins in used'
  def map_fn(x):
    current_x = get_in(['value'], x, 0)
    current_x_qty = get_in(['qty'], x, 0)
    current_y = get(current_x, frequencies_coins, -1)
    if current_y == -1:
      return x
    else:
      #print('deducted coins', current_x_qty - current_y)
      return assoc_in(x, ['qty'], current_x_qty - current_y)
    
  if is_empty(frequencies_coins):
    return source_items
  else:
    return list(map(map_fn, source_coins))

  
def update_outlet_gate(state):
  rr = get_in(vm_outlet, state, [])
  if is_not_empty(rr):
    return assoc_in(state, vm_outlet, [])
  else:
    return state

def update_recommended_items(state):
  rr = get_in(vm_recommend_items, state, {})
  if is_not_empty(rr):
    return assoc_in(state, vm_recommend_items, {})
  else:
    return state 

def return_coins(state):
  'return coins left when there is funds_amounts > 0 '
  source_coins     =  get_in(spec_coins, state)
  funds_coins       = get_in(vm_funds_coins, state)
  funds_amounts     = get_in(vm_funds_amounts, state)
  is_not_purchased  = is_collected_amounts_eq(funds_coins, funds_amounts)
  exchange_rules    = get_in(spec_coins_exchange_rules, state)
  coins_list        = maps_value(source_coins)

  if is_not_purchased:
    coins_used = funds_coins
    need_updated_coin_qty = frequencies(coins_used)
    #collect_coins = lambda state: state 
  else:
    coins_matrix          = create_coins_exchange_matrix(exchange_rules, funds_amounts)
    coins_used            = get('used', coins_matrix, [])
    need_updated_coin_qty = frequencies(coins_used)

  #print('coins to use', coins_used)
  
  collect_coins         = update_last(db_collected_coins, collect_all_coins(funds_coins))

  # thread operation
  deduct_qty            = update_last(spec_coins, deduct_coins_qty_freq(need_updated_coin_qty))
  reset_inserted_funds  = assoc_last(vm_funds_amounts, 0)
  reset_funds_coins     = assoc_last(vm_funds_coins, [])
  reset_predicted_funds = assoc_last(vm_predicted_funds, 0)
  reset_selected_items  = assoc_last(vm_selected_items, [])
  update_return_gate    = assoc_last(vm_return_gate, coins_used)
  
  new_state = thread_first(
    state,
    collect_coins,
    deduct_qty,
    update_return_gate,
    reset_inserted_funds,
    reset_funds_coins,
    reset_predicted_funds,
    reset_selected_items,
    update_outlet_gate,
    update_recommended_items
    #record transactions 
  )

  def logs(state):
    return state
  
  # before mapped coins should available matrix
  
  return thread_first(new_state,logs)


def cancel_operation(state):
  
  return state
#  
# maintenance, vault/withdraw , deposit, re-fill, re-supply,
# ==========================================================


# ==========================================================
# purchase, recommend items, transactions 
# ==========================================================


# =========================================================
# Return Coins, vault
# =========================================================


# ==========================================================
# events
# ==========================================================


# ==========================================================
# rendering, chatbots
# ==========================================================


def render():
  pass
